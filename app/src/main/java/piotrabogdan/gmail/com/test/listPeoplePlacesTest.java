package piotrabogdan.gmail.com.test;

import android.test.InstrumentationTestCase;

import piotrabogdan.gmail.com.searchapp.*;
/**
 * Testing people and places from list
 */
public class listPeoplePlacesTest extends InstrumentationTestCase {
    public void test() {
        listPeoplePlaces list = new listPeoplePlaces();
        final int countRadz = list.getMatching("Radz").size();
        final int countEk = list.getMatching("Ek").size();
        final int expectedRadz = 1, expectedEk = 5;
        assertEquals(countRadz, expectedRadz);
        assertEquals(countEk, expectedEk);
    }

    public void testGeoIntent() {
        listPeoplePlaces list = new listPeoplePlaces();
        final Places kawiarniaFromList = (Places) list.getMatching("wiarn").get(0);
        final Places newKawiarnia = new Places("Kawiarnia","22.99948365856307,72.60040283203125");
        assertEquals(kawiarniaFromList.performDefaultAction().filterHashCode(),
                newKawiarnia.performDefaultAction().filterHashCode());
    }


}
