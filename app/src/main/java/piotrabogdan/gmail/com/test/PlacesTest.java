package piotrabogdan.gmail.com.test;

import android.content.Intent;
import android.net.Uri;
import android.test.InstrumentationTestCase;

import piotrabogdan.gmail.com.searchapp.Places;

/**
 * test intent creation and pattern match
 */
public class PlacesTest extends InstrumentationTestCase {

    public void testIntent() {
        final Places kawiarnia = new Places("Kawiarnia","22.99948365856307,72.60040283203125");
        final Uri uri = Uri.parse("geo:"+"22.99948365856307,72.60040283203125");
        final Intent expected = new Intent(Intent.ACTION_VIEW, uri);
        assertTrue(kawiarnia.performDefaultAction().filterEquals(expected));
    }

    public void testPattern() {
        final Places kawiarnia = new Places("Kawiarnia","22.99948365856307,72.60040283203125");
        assertTrue(kawiarnia.checkPatternMatch("iar"));
        assertFalse(kawiarnia.checkPatternMatch("Basen"));
    }


}
