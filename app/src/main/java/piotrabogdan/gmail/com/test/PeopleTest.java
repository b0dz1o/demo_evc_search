package piotrabogdan.gmail.com.test;

import android.content.Intent;
import android.net.Uri;
import android.test.InstrumentationTestCase;

import piotrabogdan.gmail.com.searchapp.People;

/**
 * Testing people intent and pattern matching
 */
public class PeopleTest extends InstrumentationTestCase{
    public void testPattern(){
        final People mietek = new People("Mietek","123490128912");
        assertTrue(mietek.checkPatternMatch("ete"));
        assertFalse(mietek.checkPatternMatch("Pio"));
    }
    public void testIntent(){
        final People radoslaw = new People("Radosław","123490128912");
        final Uri uri = Uri.parse("tel:"+"123490128912");
        final Intent expected = new Intent(Intent.ACTION_CALL, uri);
        assertTrue(radoslaw.performDefaultAction().filterEquals(expected));
    }
}
