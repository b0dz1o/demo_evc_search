package piotrabogdan.gmail.com.searchapp;

import android.content.Intent;

/**
 * Common interface for people and places
 */
public interface Searchable {
    boolean checkPatternMatch(String pattern);

    /**Call a person or show location on map*/
    Intent performDefaultAction();
}
