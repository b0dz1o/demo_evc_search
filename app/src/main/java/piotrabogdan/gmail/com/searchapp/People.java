package piotrabogdan.gmail.com.searchapp;

import android.content.Intent;
import android.net.Uri;

/**
 * People class, containing name and phone number to call
 */
public class People implements Searchable {
    private String name, phone;

    public People(String newName, String newPhone){
        name = newName;
        phone = newPhone;
    }

    @Override
    public boolean checkPatternMatch(String pattern){
        return name.toLowerCase().contains(pattern.toLowerCase());
    }

    @Override
    public Intent performDefaultAction(){
        Intent caller = new Intent(Intent.ACTION_CALL);
        caller.setData(Uri.parse("tel:"+phone));
        return caller;
    }

    @Override
    public String toString() {
        return name;
    }

}
