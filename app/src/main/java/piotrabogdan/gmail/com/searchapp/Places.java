package piotrabogdan.gmail.com.searchapp;

import android.content.Intent;
import android.net.Uri;

/**
 * Places class, containg name and geo location
 */
public class Places implements Searchable {
    private String name, latLng;

    public Places(String newName,String newLatLng){
        name = newName;
        latLng = newLatLng;
    }
    @Override
    public boolean checkPatternMatch(String pattern) {
        return name.toLowerCase().contains(pattern.toLowerCase());
    }

    @Override
    public Intent performDefaultAction() {
        Uri uri = Uri.parse("geo:"+latLng);
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    @Override
    public String toString() {
        return name;
    }
}
