package piotrabogdan.gmail.com.searchapp;

import android.content.res.AssetManager;
import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


/**
 * wrapper for reading JSON data for people and places object creation
 */
public class readJSON {
    private InputStreamReader inputStr;

    /**
     * constructor
     *
     * @param assetMngr - passed from main activity
     * @param assetName - asset name
     */
    readJSON(AssetManager assetMngr, String assetName) {
        try {
            inputStr = new InputStreamReader(assetMngr.open(assetName));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<Searchable> generateList() {
        ArrayList<Searchable> JSONList = new ArrayList<Searchable>();
        try {
            if (inputStr.ready()) {
                JsonReader jsonReader = new JsonReader(inputStr);
                jsonReader.beginArray();
                try {
                    while (jsonReader.hasNext()) {
                        Searchable next = createInstance(jsonReader);
                        if (next != null) {
                            JSONList.add(next);
                        }
                    }
                    //jsonReader.endArray();
                } catch (IOException ex) {
                    ex.printStackTrace();
                } finally {
                    jsonReader.endArray();
                    jsonReader.close();
                    inputStr.close();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return JSONList;
    }

    private Searchable createInstance(JsonReader jsonReader) throws IOException {
        String name = null, phone = null;
        String homeVsAway = null, location = null;
        boolean instanceCreated = false;
        Searchable result = null;

        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String attribute = jsonReader.nextName();
            if (attribute.compareTo("name") == 0) {
                instanceCreated = true;
                name = jsonReader.nextString();
            } else if (attribute.compareTo("phone") == 0) {
                instanceCreated = true;
                phone = jsonReader.nextString();
            } else if (attribute.compareTo("homeTeam") == 0) {
                instanceCreated = true;
                homeVsAway = jsonReader.nextString();
            } else if (attribute.compareTo("awayTeam") == 0) {
                instanceCreated = true;
                homeVsAway += (" vs " + jsonReader.nextString());
            } else if (attribute.compareTo("point") == 0) {
                instanceCreated = true;
                location = trimLocation(jsonReader.nextString());
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        if (instanceCreated && name != null) {
            result = new People(name, phone);
        } else if (instanceCreated && location != null) {
            result = new Places(homeVsAway, location);
        }
        return result;
    }

    private String trimLocation(String rawLocation) {
        rawLocation = rawLocation.substring(rawLocation.lastIndexOf( "(" ) + 1);
        return rawLocation.substring(0, rawLocation.lastIndexOf( ")" ));
    }
}
