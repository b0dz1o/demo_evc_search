package piotrabogdan.gmail.com.searchapp;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;


/**Main activity
 *
 */
public class MainSearch extends ListActivity {

    private listPeoplePlaces dataBase;

    /**Shows all objects by default*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_search);
        AssetManager assetMngr =  getAssets();
        dataBase = new listPeoplePlaces(assetMngr);
        ArrayAdapter<Searchable> matchingAdapter =
                new ArrayAdapter<Searchable>(this, android.R.layout.simple_list_item_1,
                        dataBase.getmList());
        ListView matchingList = (ListView)findViewById(android.R.id.list);
        matchingList.setAdapter(matchingAdapter);
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            ArrayAdapter<Searchable> matchingAdapter =
                    new ArrayAdapter<Searchable>(this, android.R.layout.simple_list_item_1,
                            dataBase.getMatching(query));
            ListView matchingList = (ListView)findViewById(android.R.id.list);
            matchingList.setAdapter(matchingAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        try {
            SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        catch (NullPointerException ex){
            ex.printStackTrace();
        }
        return true;
    }

    @Override
    /**Calls a person or shows location (if map available)*/
    public void onListItemClick(ListView list, View v, int position, long id) {
        super.onListItemClick(list, v, position, id);
        Searchable s = (Searchable) list.getAdapter().getItem(position);
        Intent intent = s.performDefaultAction();
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }

}
