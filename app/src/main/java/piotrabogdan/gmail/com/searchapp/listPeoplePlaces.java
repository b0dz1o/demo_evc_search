package piotrabogdan.gmail.com.searchapp;

import android.content.res.AssetManager;

import java.util.ArrayList;

/**Common list for people and places to search through */
public class listPeoplePlaces {
    private ArrayList<Searchable> mList;

    public listPeoplePlaces(){
        mList = new ArrayList<Searchable>();
        mList.add(new People("Piotrek","1234") );
        mList.add(new People("Adam","235"));
        mList.add(new People("Zenek","12343232"));
        mList.add(new People("Mietek","123490128912"));
        mList.add(new People("Romek","123490128912"));
        mList.add(new People("Marek","123490128912"));
        mList.add(new People("Radosław","123490128912"));
        mList.add(new People("Radzimir","987"));
        mList.add(new Places("Kawiarnia","22.99948365856307,72.60040283203125"));
    }
    public listPeoplePlaces(AssetManager assetMngr){
        mList = new ArrayList<Searchable>();
        readJSON database = new readJSON(assetMngr,"people.json");
        mList.addAll(database.generateList());
        database = new readJSON(assetMngr,"places.json");
        ArrayList<Searchable> temp = database.generateList();
        mList.addAll( temp );
    }

/**
    Returns only matching ppl/places
*/
    public ArrayList<Searchable> getMatching(String pattern){
        ArrayList<Searchable> matchingPeople = new ArrayList<Searchable>();
        for (Searchable man : mList){
            if(man.checkPatternMatch(pattern)){
                matchingPeople.add(man);
            }
        }
        return matchingPeople;
    }

    /**To show all when no search is pending*/
    ArrayList<Searchable> getmList() {
        return mList;
    }

}
